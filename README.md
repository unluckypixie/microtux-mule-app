# README #

### What is this project? ###

This maven pom project simplifies making new mule apps, just add it as the parent project to your project:


```
#!xml
	<parent>
		<groupId>uk.co.microtux.lib</groupId>
		<artifactId>mule-app</artifactId>
		<version>3.6.0-1-SNAPSHOT</version>
	</parent>

```

The version of this project follows the Mule runtime version that it works with, i.e. <MULE-VERSION>-<RELEASE>, so the version 3.6.0-1-SNAPSHOT would be the first release for mule runtime 3.6.0.

### Usage ###

The eclipse plugin in this project is configured to generate mule app project files:

```
#!bash
mvn eclipse:clean eclipse:eclipse

```

Should generate a `mule-project.xml` file.